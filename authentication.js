var uuid = require('node-uuid');
var JSONStream = require('JSONStream');
var express = require('express');
var router = express.Router();

router.post('/signin', function (req, res) {
    console.log("Login ");
    var collection = req.collection;
    var data = {
        email: req.headers.email,
        password: req.headers.password
    };
    collection.findOne({email: data.email,password:data.password}).then((val)=>{
        if(val != null){
            val['status'] = true;
            res.send({
                data:val,
                textStatus:"SignIn Susccessfully",
                request:null
            })
        }
        else {
            res.sendStatus(400).send({ms:"Not Fond"})
        }
    })
});

router.post('/signup', function (req, res) {
    console.log("Sign Up")
    var collection = req.collection;
    var data = {
        fullname: req.headers.fullname,
        email: req.headers.email,
        password: req.headers.password
    };
    collection.findOne({email: data.email}).then((val) => {
        if (val == null) {
            collection.insert(data);
            val['status'] = true;
            res.send({
                data:val,
                textStatus:"SignUp Susccessfully",
                request:null
            })
        } else {
            res.sendStatus(400).send({ms:"Not Fond"})
        }
    });
});

module.exports = router;