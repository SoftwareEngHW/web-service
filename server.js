var express = require('express');
var rewriter = require('express-urlrewrite')
var bodyParser = require('body-parser');
var cors = require('cors');
var MongoClient = require('mongodb').MongoClient;
var ip = require("ip");
var JSONStream = require('JSONStream');

var route = require('./route');
var auth = require('./authentication');
var config = require('./config');

var mongodb = {};

var app = express();

app.use(cors());
app.use(bodyParser.json({
    limit: '50mb'
}));
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use('*', function (req, res, next) {
    res.set({
        'Access-Control-Expose-Headers': 'Authorization',
        'Content-Type': 'application/json; charset=utf-8',
        'Bind-Address': ip.address() + ':' + config.port
    });
    next();
});

app.param('db', function (req, res, next, value) {
    req.mongodb = mongodb[value];
    next();
});

app.param('collection', function (req, res, next, value) {
    req.collection = req.mongodb.db.collection(value);
    next();
});

app.post('/signin', rewriter('/authentication/cores/user_db/signin'));
app.post('/signup', rewriter('/authentication/cores/user_db/signup'));
app.post('/resetpassword', rewriter('/authentication/cores/user_db/resetpassword'));
app.use('/authentication/:db/:collection', auth);

app.use('/mongodb/:db/:collection', route);

app.get('/servertime', function (req, res) {
    var long_date = new Date().getTime()
    res.send(long_date.toString());
});

var count = config.mongodb.length;

config.mongodb.forEach(function (db_config) {
    MongoClient.connect(db_config.url,
        function (err, db) {
            if (!err) {
                count--;
                mongodb[db_config.db] = {
                    'db': db,
                    'config': db_config
                };
                if (count == 0) {
                    app.listen(config.port, function () {
                        console.log('Server listening on port %d', this.address().port);
                    });
                }
            }
        });
});